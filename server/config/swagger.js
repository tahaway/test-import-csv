const {
  randomID,
} = require('../api/utils/general');
var md5 = require('md5');

const code400 = {
  description: 'Invalid body payload.',
  type: 'object',
  properties: {
    statusCode: {
      type: 'number'
    },
    error: {
      type: 'string'
    },
    status: {
      type: 'string'
    },
    message: {
      type: 'string'
    },
  },
  example: {
    statusCode: 400,
    error: 'Bad Request',
    status: 'fail',
    message: 'Body message error',
  }
};

const code409 = {
  description: 'Failure response',
  type: 'object',
  properties: {
    id: {
      type: 'string'
    },
    statusCode: {
      type: 'number'
    },
    error: {
      type: 'string'
    },
    status: {
      type: 'string'
    },
    message: {
      type: 'string'
    },
  },
  example: {
    id: randomID('failure'),
    status: 'fail',
    error: 'Conflict',

    message: 'Error Message',
  }
}

const code200SuccessTimeSheet = {
  description: 'Successful response',
  type: 'object',
  properties: {
    id: {
      type: 'string'
    },
    status: {
      type: 'string'
    }
  },
  example: {
    id: randomID('request'),
    status: 'success'
  }
}

const code200Message = {
  description: 'Successful response',
  type: 'object',
  properties: {
    id: {
      type: 'string'
    },
    status: {
      type: 'string'
    },
    message: {
      type: 'string'
    },
  },
  example: {
    id: randomID('request'),
    status: 'success',
    message: 'Message for operation',
  }
}


module.exports = {
  code400,
  code409,
  code200Message,
};