const nconf = require('nconf');
const server = require('./server')
const {
    loadSettings
} = require('./config/configurationAdaptor')
const appSettingsPath = "./config/appSettings.json"

loadSettings({
        appSettingsPath
    })
    .then(() => {
        // Read the config property required for starting the server
        const serverOptions = {
            app: nconf.get('app'),
            fastify: nconf.get('fastify'),
            logging: nconf.get('logging'),
            swagger: nconf.get('swagger')
        };
        server.createServer(serverOptions);
        // TODO Start the server
    })
    .catch((err) => {
        console.log(err);
    })