require('dotenv').config();
const Fastify = require('fastify');
const path = require('path');
const AutoLoad = require('fastify-autoload');
global.appRoot = path.resolve(__dirname);

const createServer = (options) => {
    
    const {
        logging,
        app,
        fastify,
        swagger
    } = options;

    if(process.env.HOST && process.env.PORT){
        app.host = process.env.HOST
        app.port = process.env.PORT
    }

    // create the server
    const server = Fastify({
        logger: logging ? fastify.config.logs : logging,
        ignoreTrailingSlash: true,
        useUnifiedTopology: true,
    });

    
    server
        .register(require('fastify-swagger'), {
            routePrefix: '/documentation',
            swagger: swagger,
            exposeRoute: true
        })

    server.register(require('fastify-multipart'), {
            addToBody: true,
            limits: {
                fieldNameSize: 5, // Max field name size in bytes
                files: 1,           // Max number of file fields
            }
        });

    server
        .register(AutoLoad, {
            dir: path.join(__dirname, 'api', 'routes')
        });

    server.register(require('fastify-cors'), { 
        origin: true,
        methods: "GET,HEAD,PUT,PATCH,POST,DELETE"
    })

    // start the server
    server.listen(app.port, app.host, (err) => {
        if (err) {
            server.log.error(err);
            console.log(err);
            process.exit(1);
        }
        console.log("API End-point is up at http://" + app.host + ":" + app.port);
        console.log("API Swagger Documentation at http://" + app.host + ":" + app.port + '/documentation/');

        server.log.info('Server Started');
    });
    return server;
}

module.exports = {
    createServer
}