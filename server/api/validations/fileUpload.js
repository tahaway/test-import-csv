const {
    code400,
    code401,
    code409,
    code200Message,
} = require('../../config/swagger')


const validateFileUpload = {
    schema: {
        description: 'Upload CSV File.',
        tags: ['file'],
        response: {
            400: code400,
            200: code200Message,
            409: code409
        }
    }
}


module.exports = {
    validateFileUpload
}