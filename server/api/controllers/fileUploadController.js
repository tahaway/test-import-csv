const {
    success,
    error,
    randomID,
    csvInjector
} = require('../utils/general');
const {
    FILE_NOT_MULTIPART,
    FILE_MIME_TYPE_ALLOWED_MESSAGE,
} = require('../models/common/Errors');
const db = require('../models');
const Product = db.Product;

const uploadFile = async (req, res) => {
    try {
        // unit testing payload body blank patch.
        if(JSON.stringify(req.body) === '{}' || JSON.stringify(req.body) === 'null'){
            return error(res, FILE_NOT_MULTIPART);
        } 

        // unit testing payload file blank patch.
        if(JSON.stringify(req.body.file) === '{}' || JSON.stringify(req.body.file) === 'null'){
            return error(res, FILE_NOT_MULTIPART);
        } 


        // unit testing payload mimetype.
        if (req.body.file[0].mimetype !== 'text/csv' ){
            return error(res, FILE_MIME_TYPE_ALLOWED_MESSAGE)
        }

        // Import File Buffer and Convert to Buffer to dataArray
        const newFilePath = appRoot + '/files/' + randomID('file');
        const csvData = await csvInjector(req.body.file[0], newFilePath);

        // Insert and upsert the Data to Table with unique field handler
        const fields = ["code", "sku", "title", "description"];
        
        await Product.bulkCreate(csvData, {
            updateOnDuplicate: fields
        });

        return success(true, res);
    } catch (err) {
        console.log("error",err);
        return error(res, FILE_NOT_MULTIPART);
    }
}

module.exports = {
    uploadFile
}