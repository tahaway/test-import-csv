module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define('Product', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    title: { type: DataTypes.STRING, allowNull: true, unique: false },
    code: { type: DataTypes.STRING, allowNull: true, unique: true },
    sku: { type: DataTypes.STRING, allowNull: false, unique: false  },
    description : { type: DataTypes.STRING, allowNull: true },
  }, {});
  return Product;
};