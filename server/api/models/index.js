'use strict';
const fs        = require('fs');
const path      = require('path');
const Sequelize = require('sequelize');
const nconf     = require('nconf');
let db          = {};
let config      = nconf.get('PostgreSQL.config');

module.exports = db;

initialize();

async function initialize() {
    // Connection loading 
    let sequelize = new Sequelize(nconf.get('PostgreSQL.database'), nconf.get('PostgreSQL.user'), nconf.get('PostgreSQL.password'), config);

    db.sequelize = sequelize;
    db.Sequelize = Sequelize;
    db.Product = require("./Product.js")(sequelize, Sequelize);

    // sync all models with database
    await sequelize.sync();
}