const APPLICATION_ERROR = "API facing problem to handle this request.";
const FILE_NOT_MULTIPART = "Only multipart file upload request is allowed on field 'file'.";
const FILE_MIME_TYPE_ALLOWED_MESSAGE = "Only text/csv mimetype allowed.";

module.exports = {
  APPLICATION_ERROR,
  FILE_NOT_MULTIPART,
  FILE_MIME_TYPE_ALLOWED_MESSAGE,
};