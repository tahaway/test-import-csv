const {  uploadFile } = require('../controllers/fileUploadController')
const {  validateFileUpload } = require('../validations/fileUpload');

module.exports = async function (fastify,opts) {

    fastify.route({
        method: 'POST',
        url: '/v1/api/file/upload/',
        schema : validateFileUpload.schema,
        handler : uploadFile
    })

  }