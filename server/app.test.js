'use strict'
const {
    test
} = require('tap')
const nconf = require('nconf');
const supertest = require('supertest')
const build = require('./server')
const {
    loadSettings
} = require('./config/configurationAdaptor')
const appSettingsPath = "./config/appSettings.json"

test('requests the "/v1/api/file/upload/" GET route', async t => {

    loadSettings({
            appSettingsPath
        })
        .then(async () => {
            // Read the config property required for starting the server

            const serverOptions = {
                app: nconf.get('app'),
                fastify: nconf.get('fastify'),
                logging: nconf.get('logging'),
            };

            const fastify = await build.createServer(serverOptions);
            t.tearDown(() => fastify.close())

            const newrequest = await supertest(fastify.server)
                .get('/v1/api/file/upload/')
                .expect(404)
                .expect('Content-Type', 'application/json; charset=utf-8')

        })
})

test('requests the "/v1/api/file/upload/" POST route', async t => {

    loadSettings({
            appSettingsPath
        })
        .then(async () => {
            // Read the config property required for starting the server

            const serverOptions = {
                app: nconf.get('app'),
                fastify: nconf.get('fastify'),
                logging: nconf.get('logging'),
            };

            const fastify = await build.createServer(serverOptions);
            t.tearDown(() => fastify.close())

            const newrequest = await supertest(fastify.server)
                .post('/v1/api/file/upload/')
                .expect(409)
                .expect('Content-Type', 'application/json; charset=utf-8')

        })
})


test('requests the "/v1/api/file/upload/" POST Payload route', async t => {

    loadSettings({
            appSettingsPath
        })
        .then(async () => {
            // Read the config property required for starting the server

            const serverOptions = {
                app: nconf.get('app'),
                fastify: nconf.get('fastify'),
                logging: nconf.get('logging'),
            };

            const fastify = await build.createServer(serverOptions);
            t.tearDown(() => fastify.close())

            const newrequest = await supertest(fastify.server)
                .post('/v1/api/file/upload/')
                .send({ file : {} })
                .expect(409)
                .expect('Content-Type', 'application/json; charset=utf-8')

        })
})
