import React, { useState } from "react";
import UploadService from "../services/FileUpload";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Form, Container, Row, Button, Col, Jumbotron, ProgressBar, Alert } from 'react-bootstrap'

const UploadFiles = () => {
  const [selectedFiles, setSelectedFiles] = useState(undefined)
  const [currentFile, setCurrentFile] = useState(undefined);
  const [progress, setProgress] = useState(0);
  const [message, setMessage] = useState("");

  const selectFile = (event) => {
    setSelectedFiles(event.target.files);
  };

  const upload = () => {
    let currentFile = selectedFiles[0];

    setProgress(0);
    setMessage("");
    setCurrentFile(currentFile);

    UploadService.upload(currentFile, (event) => {
      setProgress(Math.round((100 * event.loaded) / event.total));
    })
      .then((response) => {
        setMessage(response.data.message);
        return true
      })
      .catch(() => {
        setProgress(0);
        setMessage("Could not upload the file!");
        setCurrentFile(undefined);
      });

    setSelectedFiles(undefined);
  };

  return (
    <Container fluid="md">
  <Row>
    <Col>
    <Jumbotron>
      <h1>Import Data via CSV File!</h1>
      <p>
        Require fields are code, sku, title, description, Database Schema consider "code" field as unique identifier,
        you can change unique identifier from server APP ("api/model/Product.js"), on basis off code upsert operation perform.
      </p>
    </Jumbotron>
      <Form.File 
        id="custom-file"
        label="Attachment"
        onChange={selectFile}
        accept="text/csv"
      />
      {message && (
        <Alert key={'error'} variant={'danger'} className={'mt-1'}>
            {message}
        </Alert>
      )}
      <hr />
      <div>
      <Button variant="primary" type="submit" disabled={!selectedFiles}  onClick={upload}>
        Upload
      </Button>
      </div>
      {currentFile && (<div> <hr /> <ProgressBar now={progress} label={ progress > 99 ? 'File Uploaded' : `File Uploading Progress ${progress}%`} /></div>)}

      <div className="alert alert-light" role="alert">
        {message}
      </div>
      </Col>
      </Row>
    </Container>
  );
};

export default UploadFiles;