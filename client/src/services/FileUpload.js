import http from "./RequestHttp";

const upload = (file, onUploadProgress) => {
  let formData = new FormData();

  formData.append("file", file);

  return http.post("/v1/api/file/upload/", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
    onUploadProgress,
  });
};

/* eslint import/no-anonymous-default-export: [2, {"allowObject": true}] */
export default {
    upload
};