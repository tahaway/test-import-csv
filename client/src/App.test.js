import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn Import Data', () => {
  render(<App />);
  const headingElement = screen.getByText(/Import Data/i);
  expect(headingElement).toBeInTheDocument();
});
